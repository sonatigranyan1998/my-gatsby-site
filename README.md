Packaging and deployment


To deploy the connector you can simply use the Docker deployment.
First you need to build the image with some parameters you should define previously. Replace
<Target Bucket> with the S3 bucket where the CloudFormation template shall be stored.
<Lambda Bucket> with the S3 bucket where the Lambda deployment package shall be stored.
<AWS Access Key> with your AWS access key (e.g. AKIAXXXXXXXXXXXXXXXQ)
<AWS Secret Access Key> with your AWS secret access key (e.g. NMAXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXR)
<Region> with the region you want to deploy the stack in
