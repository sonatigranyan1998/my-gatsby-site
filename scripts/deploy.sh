#!/bin/bash
ls
printf "hello\nworld1\n"
export DEBIAN_FRONTEND=noninteractive
apt -yq update 
apt install -yq python
apt install -yq python3-pip
pip3 install awscli
uname -r
printf "hello\nworld2\n"
cp appspec.yml public/
cp -r scripts public/
cp package.json public/
printf "hello\nworld3\n"
tar -cvf SampleApp.tar.gz public/
printf "hello\nworld4\n"
ls
aws s3 cp SampleApp.tar.gz s3://$S3_BUCKET/
